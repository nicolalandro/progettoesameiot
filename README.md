# Where is my ...?
This is the prototype of a project in with a drone or a capera scan a room and ask if there it can find the object that you searching for.
For example "where is my umbrella?", the software scan the room and search for the umbrella using Yolo. After it publish on mqtt.

## Architecture idea

![architecture](imgs/ArchitectureIotWhereIsMy.png)

## How to use

```
cd prototype
docker-compose up

firefox http://localhost:8081
# create db whereismy
# create collection cameras

firefox http://localhost:8888/notebooks/notebook/RegisterCam.ipynb
# Add a camera name

firefox http://localhost:8888/notebooks/notebook/Camera.ipynb
# Register the camera simulator

firefox http://localhost:8888/notebooks/notebook/User%20Request.ipynb
# request for something

```

# Study
* nodejs
  * nodered
  * cryptojs
  * mqtt
  * MongoDB?
* Mongo DB
* MQTT
  * Mosquitto
* python
  * [MQTT Beginners Guide with python example](https://medium.com/python-point/mqtt-basics-with-python-examples-7c758e605d4)
  * [opencv](https://pypi.org/project/opencv-python/)
  * [QrCode python](https://gitlab.com/nicolalandro/encoded_text_to_img/-/tree/master/use_qr_code)
  * [pyCripto](https://pypi.org/project/pycrypto/)
  * [YoloV5 small](https://colab.research.google.com/drive/1W-VhJkXtegEzxm7kvVPWgJlo9eejMopA)
  * [Pymongo](https://www.tutorialkart.com/mongodb/connect-to-mongodb-from-python/)
  * [Pymongo Auth doc](https://pymongo.readthedocs.io/en/stable/examples/authentication.html#percent-escaping-username-and-password)
  * [Jupyter widget](https://medium.com/@jdchipox/how-to-interact-with-jupyter-33a98686f24e)
  * [Tello python](https://github.com/dji-sdk/Tello-Python) and [my examples](https://gitlab.com/nicolalandro/tello_code_sample), controll a drone with python
